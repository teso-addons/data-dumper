local ItemBrowserData = ItemBrowser.data

-- /script d(GetActiveCollectibleByType(COLLECTIBLE_CATEGORY_TYPE_COMPANION))
-- local COMPANIONS_IDS = {
-- 	9245, -- Bastian
-- 	9353, -- Mirri
-- 	9911, -- Ember
-- 	9912, -- Isobel Veloise
-- }

local colors = {
    health = ZO_ColorDef:New(GetInterfaceColor(INTERFACE_COLOR_TYPE_POWER, POWERTYPE_HEALTH)),
    magicka = ZO_ColorDef:New(GetInterfaceColor(INTERFACE_COLOR_TYPE_POWER, POWERTYPE_MAGICKA)),
    stamina = ZO_ColorDef:New(GetInterfaceColor(INTERFACE_COLOR_TYPE_POWER, POWERTYPE_STAMINA)),
    violet = ZO_ColorDef:New(GetInterfaceColor(INTERFACE_COLOR_TYPE_ITEM_QUALITY_COLORS, ITEM_DISPLAY_QUALITY_ARTIFACT)),
    gold = ZO_ColorDef:New(GetInterfaceColor(INTERFACE_COLOR_TYPE_ITEM_QUALITY_COLORS, ITEM_DISPLAY_QUALITY_LEGENDARY)),
    mythic = ZO_ColorDef:New(GetInterfaceColor(INTERFACE_COLOR_TYPE_ITEM_QUALITY_COLORS, ITEM_DISPLAY_QUALITY_MYTHIC_OVERRIDE)),
    brown = ZO_ColorDef:New("885533"),
    teal = ZO_ColorDef:New("66CCCC"),
    pink = ZO_ColorDef:New("FF99CC")
}

local zoneTypeNames = {
    [1] = GetString("SI_ITEMBROWSER_FILTERDROP", 4),
    [2] = GetString("SI_ITEMBROWSER_FILTERDROP", 5),
    [3] = GetString("SI_ITEMBROWSER_FILTERDROP", 6),
    [4] = GetString("SI_ITEMBROWSER_FILTERDROP", 7)
}

local curLang = GetCVar("language.2")

local defaults = {sets = {}, skills = {}, compskills_raw = {}, compskills = {}, champ = {}}
local vars = {sets = {}, skills = {}, champ = {}, compskills_raw = {}, compskills = {}}

local function GetNextAbilityMechanicFlagIter(abilityId) return function(_, lastFlag) return GetNextAbilityMechanicFlag(abilityId, lastFlag) end end

local function CheckFlag(flags, flagToCheck) return (BitAnd(flags, flagToCheck) == flagToCheck) end

local function GetZoneNameById(zoneId)
    if (zoneId < -100) then
        return (ItemBrowserData.specialNames[zoneId])
    elseif (zoneId < 0) then
        return (GetString("SI_ITEMBROWSER_SOURCE_SPECIAL", zoneId * -1))
    else
        return (LocalizeString("<<C:1>>", GetZoneNameByIndex(GetZoneIndex(zoneId))))
    end
end

local function MakeItemLink(id, flags, ext)
    local quality = 364
    local crafted = 0
    local health = 10000

    if (CheckFlag(flags, ItemBrowserData.flags.crafted)) then
        quality = 370
        crafted = 1
    elseif (CheckFlag(flags, ItemBrowserData.flags.jewelry)) then
        health = 0
    elseif (CheckFlag(flags, ItemBrowserData.flags.weapon) and not CheckFlag(flags, ItemBrowserData.flags.shield)) then
        health = 500
    end

    local style = ITEMSTYLE_NONE

    if (CheckFlag(flags, ItemBrowserData.flags.allianceStyle)) then
        style = ITEMSTYLE_ALLIANCE_ALDMERI
    elseif (CheckFlag(flags, ItemBrowserData.flags.multiStyle)) then
        style = ITEMSTYLE_RACIAL_IMPERIAL
    elseif (CheckFlag(flags, ItemBrowserData.flags.manualStyle)) then
        style = ext
    end

    local itemLink = string.format("|H1:item:%d:%d:50:0:0:0:0:0:0:0:0:0:0:0:0:%d:%d:0:0:%d:0|h|h", id, quality, style, crafted, health)

    if (crafted == 1) then
        -- Attach an enchantment to crafted gear

        local enchantments = {[ARMORTYPE_NONE] = 0, [ARMORTYPE_HEAVY] = 26580, [ARMORTYPE_LIGHT] = 26582, [ARMORTYPE_MEDIUM] = 26588}

        itemLink = itemLink:gsub("370:50:0:0:0", string.format("370:50:%d:370:50", enchantments[GetItemLinkArmorType(itemLink)]))
    end

    return (itemLink)
end

local function GetSetBonuses(itemLink, numBonuses)
    local bonuses = {}
    for i = 1, numBonuses do bonuses[i] = select(2, GetItemLinkSetBonusInfo(itemLink, false, i)) end
    return bonuses
end

local function CreateEntryFromRaw(rawEntry)
    local id = rawEntry[1]
    local flags = rawEntry[2]

    local itemLink = MakeItemLink(id, flags, rawEntry[4])
    local subname, itemType, color
    local zoneType = {}

    local _, name, bonuses, _, maxEquipped, setId = GetItemLinkSetInfo(itemLink)
    name = LocalizeString("<<C:1>>", name)
    subname = rawEntry.alt or ""

    if (CheckFlag(flags, ItemBrowserData.flags.crafted)) then
        itemType = string.format("%s (%d)", GetString(SI_ITEMBROWSER_TYPE_CRAFTED), rawEntry[4])
        color = colors.pink
        zoneType[0] = true
    elseif (CheckFlag(flags, ItemBrowserData.flags.mythic)) then
        itemType = GetString("SI_ITEMDISPLAYQUALITY", ITEM_DISPLAY_QUALITY_MYTHIC_OVERRIDE)
        color = colors.mythic
    elseif (CheckFlag(flags, ItemBrowserData.flags.jewelry)) then
        itemType = GetString("SI_GAMEPADITEMCATEGORY", GAMEPAD_ITEM_CATEGORY_JEWELRY)
        color = colors.violet
    elseif (CheckFlag(flags, ItemBrowserData.flags.monster)) then
        itemType = GetString(SI_ITEMBROWSER_TYPE_MONSTER)
        color = colors.brown
    elseif (CheckFlag(flags, ItemBrowserData.flags.weapon)) then
        subname = LocalizeString("<<t:1>>", GetItemLinkName(itemLink))
        itemType = LocalizeString("<<C:1>>", GetString("SI_ITEMTYPE", ITEMTYPE_WEAPON))
        color = colors.gold
    elseif (CheckFlag(flags, ItemBrowserData.flags.mixedWeights)) then
        itemType = GetString("SI_DYEHUECATEGORY", DYE_HUE_CATEGORY_MIXED)
        color = colors.teal
    else
        armorType = GetItemLinkArmorType(itemLink)

        local armorColors = {
            [ARMORTYPE_NONE] = ZO_DEFAULT_TEXT,
            [ARMORTYPE_HEAVY] = colors.health,
            [ARMORTYPE_LIGHT] = colors.magicka,
            [ARMORTYPE_MEDIUM] = colors.stamina
        }

        itemType = LocalizeString("<<C:1>>", GetString("SI_ARMORTYPE", armorType))
        color = armorColors[armorType]
    end

    local sources, zoneIds = {}, {}
    for _, source in ipairs(rawEntry[3]) do
        if (type(source) == "number") then
            table.insert(sources, GetZoneNameById(source))
            zoneIds[source] = true
            zoneType[ItemBrowserData.zoneClassification[source]] = true
        elseif (type(source) == "table") then
            local subSources = {}
            for _, subSource in ipairs(source) do
                table.insert(subSources, GetZoneNameById(subSource))
                zoneIds[subSource] = true
                if ItemBrowserData.zoneClassification[subSource] then zoneType[ItemBrowserData.zoneClassification[subSource]] = true end
            end
            sources[#sources] = string.format("%s (%s)", sources[#sources], table.concat(subSources, ", "))
        end
    end

    zoneType[(GetItemLinkBindType(itemLink) == BIND_TYPE_ON_EQUIP) and 6 or 7] = true

    return ({
        name = name,
        subname = subname,
        itemType = itemType,
        source = table.concat(sources, ", "),
        zoneIds = zoneIds,
        zoneType = zoneType,
        color = color,
        bonuses = GetSetBonuses(itemLink, bonuses),
        itemLink = itemLink,
        setId = setId,
        setSize = GetNumItemSetCollectionPieces(setId),
        setFound = setFound,
        maxEquipped = maxEquipped,
        icon = GetItemLinkIcon(itemLink)
    })
end

local function GetZoneTypeName(zoneTypes)
    local result = {}
    for i, _ in pairs(zoneTypes) do if (zoneTypeNames[i]) then table.insert(result, zoneTypeNames[i]) end end
    return #result > 0 and result[1] or nil
end

local function GetAbilityNewEffects(...)
    local numNewEffectReturns = select("#", ...)
    if (numNewEffectReturns > 0) then
        local result = {}
        for i = 1, numNewEffectReturns do
            local newEffect = select(i, ...)
            table.insert(result, newEffect)
        end
        return result
    end
end

local function _dumpChampPoints()
    vars.champ[curLang] = {}

    for _, championDisciplineData in CHAMPION_DATA_MANAGER:ChampionDisciplineDataIterator() do
        for _, championSkillData in championDisciplineData:ChampionSkillDataIterator() do
            local bonusTexts = {}
            if championSkillData:HasJumpPoints() then
                for ji, jumpPointNum in ipairs(championSkillData:GetJumpPoints()) do
                    bonusTexts[ji] = GetChampionSkillCurrentBonusText(championSkillData:GetId(), jumpPointNum)
                end
            else
                for ji = 1, championSkillData:GetMaxPossiblePoints() do bonusTexts[ji] = GetChampionSkillCurrentBonusText(championSkillData:GetId(), ji) end
            end
            table.insert(vars.champ[curLang], {
                disciplineId = championDisciplineData:GetId(),
                disciplineName = championDisciplineData:GetFormattedName(),
                abilityId = championSkillData:GetAbilityId(),
                name = championSkillData:GetFormattedName(),
                description = championSkillData:GetDescription(),
                bonusTexts = bonusTexts,
                maxPoints = championSkillData:GetMaxPossiblePoints(),
                jumpPoints = championSkillData:GetJumpPoints(),
                isSlottable = championSkillData:IsTypeSlottable(),
                slottableText = (championSkillData:IsTypeSlottable() and GetString(SI_CHAMPION_TOOLTIP_SLOT_TO_ACTIVATE) or nil)
            })
        end

        -- for _, championClusterData in championDisciplineData:ChampionClusterDataIterator() do
        -- d(championClusterData:GetFormattedName())
        -- end
    end
end

local function _dumpSkillsFrom(DATA_MANAGER, dest)
    for _, skillTypeData in DATA_MANAGER:SkillTypeIterator() do
        local lineTypeIcon, _, _ = skillTypeData:GetKeyboardIcons()
        for _, skillLineData in skillTypeData:SkillLineIterator() do
            for _, skillData in skillLineData:SkillIterator() do
                if not skillData:IsPassive() then
                    local triple = {}
                    local baseProgressionData
                    local morph1ProgressionData
                    local morph2ProgressionData

                    if skillData:IsCompanionSkill() then
                        baseProgressionData = skillData:GetProgressionData(MORPH_SLOT_BASE)
                        triple = {baseProgressionData}
                    else
                        baseProgressionData = skillData:GetProgressionData(MORPH_SLOT_BASE)
                        morph1ProgressionData = skillData:GetProgressionData(MORPH_SLOT_MORPH_1)
                        morph2ProgressionData = skillData:GetProgressionData(MORPH_SLOT_MORPH_2)
                        triple = {baseProgressionData, morph1ProgressionData, morph2ProgressionData}
                    end

                    for _, progressionData in ipairs(triple) do
                        local abilityId = progressionData:GetAbilityId()
                        local descriptionHeader = GetAbilityDescriptionHeader(abilityId)
                        local parentAbilityId = nil
                        local casterTag = nil
                        if skillData:IsCompanionSkill() then
                            casterTag = "companion"
                        else
                            parentAbilityId = (progressionData:IsMorph() and baseProgressionData:GetAbilityId() or nil)
                        end

                        -- Cast Time
                        local castType, castTimeValue
                        local channeled, castTime, channelTime = GetAbilityCastInfo(abilityId, 4, casterTag)
                        if channeled then
                            castType = GetString(SI_ABILITY_TOOLTIP_CHANNEL_TIME_LABEL)
                            castTimeValue = ZO_FormatTimeMilliseconds(channelTime, TIME_FORMAT_STYLE_CHANNEL_TIME, TIME_FORMAT_PRECISION_TENTHS_RELEVANT,
                                                                      TIME_FORMAT_DIRECTION_NONE)
                        else
                            castType = GetString(SI_ABILITY_TOOLTIP_CAST_TIME_LABEL)
                            castTimeValue = ZO_FormatTimeMilliseconds(castTime, TIME_FORMAT_STYLE_CAST_TIME, TIME_FORMAT_PRECISION_TENTHS_RELEVANT,
                                                                      TIME_FORMAT_DIRECTION_NONE)
                        end

                        -- Target
                        local targetType
                        local targetValue = GetAbilityTargetDescription(abilityId, 4, casterTag)
                        if targetValue then targetType = GetString(SI_ABILITY_TOOLTIP_TARGET_TYPE_LABEL) end

                        -- Range
                        local rangeType, rangeValue
                        local minRangeCM, maxRangeCM = GetAbilityRange(abilityId, 4, casterTag)
                        if maxRangeCM > 0 then
                            rangeType = GetString(SI_ABILITY_TOOLTIP_RANGE_LABEL)
                            if minRangeCM == 0 then
                                rangeValue = zo_strformat(SI_ABILITY_TOOLTIP_RANGE, FormatFloatRelevantFraction(maxRangeCM / 100))
                            else
                                rangeValue = zo_strformat(SI_ABILITY_TOOLTIP_MIN_TO_MAX_RANGE, FormatFloatRelevantFraction(minRangeCM / 100),
                                                          FormatFloatRelevantFraction(maxRangeCM / 100))
                            end
                        end

                        -- Radius/Distance
                        local distanceType, distanceValue
                        local radiusCM = GetAbilityRadius(abilityId, 4, casterTag)
                        local angleDistanceCM = GetAbilityAngleDistance(abilityId)
                        if radiusCM > 0 then
                            if angleDistanceCM > 0 then
                                distanceType = GetString(SI_ABILITY_TOOLTIP_AREA_LABEL)
                                distanceValue = zo_strformat(SI_ABILITY_TOOLTIP_AOE_DIMENSIONS, FormatFloatRelevantFraction(radiusCM / 100),
                                                             FormatFloatRelevantFraction(angleDistanceCM * 2 / 100))
                            else
                                distanceType = GetString(SI_ABILITY_TOOLTIP_RADIUS_LABEL)
                                distanceValue = zo_strformat(SI_ABILITY_TOOLTIP_RADIUS, FormatFloatRelevantFraction(radiusCM / 100))
                            end
                        end

                        -- Duration
                        local durationType, durationValue
                        if IsAbilityDurationToggled(abilityId) then
                            durationType = GetString(SI_ABILITY_TOOLTIP_DURATION_LABEL)
                            durationValue = GetString(SI_ABILITY_TOOLTIP_TOGGLE_DURATION)
                        else
                            local durationMS = GetAbilityDuration(abilityId, 4, casterTag)
                            if durationMS > 0 then
                                durationType = GetString(SI_ABILITY_TOOLTIP_DURATION_LABEL)
                                durationValue = ZO_FormatTimeMilliseconds(durationMS, TIME_FORMAT_STYLE_DURATION, TIME_FORMAT_PRECISION_TENTHS_RELEVANT,
                                                                          TIME_FORMAT_DIRECTION_NONE)
                            end
                        end

                        -- Cooldown
                        local cooldownType, cooldownValue
                        local cooldownMS = GetAbilityCooldown(abilityId, casterTag)
                        if cooldownMS > 0 then
                            cooldownType = GetString(SI_ABILITY_TOOLTIP_COOLDOWN)
                            cooldownValue = ZO_FormatTimeMilliseconds(cooldownMS, TIME_FORMAT_STYLE_DURATION, TIME_FORMAT_PRECISION_TENTHS_RELEVANT,
                                                                      TIME_FORMAT_DIRECTION_NONE)
                        end

                        -- Cost
                        local costsTable = {}
                        for flag in GetNextAbilityMechanicFlagIter(abilityId) do
                            local cost = GetAbilityCost(abilityId, flag, 4)
                            if cost > 0 then
                                local mechanicName = GetString("SI_COMBATMECHANICFLAGS", flag)
                                local costString = zo_strformat(SI_ABILITY_TOOLTIP_RESOURCE_COST, cost, mechanicName)
                                table.insert(costsTable, {flag = flag, cost = cost, mechanicName = mechanicName, costString = costString})
                            end
                        end

                        for flag in GetNextAbilityMechanicFlagIter(abilityId) do
                            local cost, chargeFrequencyMS = GetAbilityCostOverTime(abilityId, flag, 4)
                            if cost > 0 then
                                local mechanicName = GetString("SI_COMBATMECHANICFLAGS", flag)
                                local formattedChargeFrequency = ZO_FormatTimeMilliseconds(chargeFrequencyMS, TIME_FORMAT_STYLE_SHOW_LARGEST_UNIT,
                                                                                           TIME_FORMAT_PRECISION_TENTHS_RELEVANT, TIME_FORMAT_DIRECTION_NONE)
                                local costOverTimeString = zo_strformat(SI_ABILITY_TOOLTIP_RESOURCE_COST_OVER_TIME, cost, mechanicName, formattedChargeFrequency)
                                table.insert(costsTable, {
                                    flag = flag,
                                    cost = cost,
                                    mechanicName = mechanicName,
                                    formattedChargeFrequency = formattedChargeFrequency,
                                    costOverTimeString = costOverTimeString,
									chargeFrequencyMS = chargeFrequencyMS
                                })
                            end
                        end

                        -- Roles
                        local rolesTable = {}
                        local rolesType, rolesValue
                        local isTankRole, isHealerRole, isDamageRole = GetAbilityRoles(abilityId)
                        if isTankRole then table.insert(rolesTable, GetString(SI_LFGROLE2)) end
                        if isHealerRole then table.insert(rolesTable, GetString(SI_LFGROLE4)) end
                        if isDamageRole then table.insert(rolesTable, GetString(SI_LFGROLE1)) end
                        if #rolesTable > 0 then
                            rolesType = GetString(SI_ABILITY_TOOLTIP_ROLE_LABEL)
                            rolesValue = rolesTable
                        end

                        table.insert(dest[curLang], {
                            lineTypeId = skillTypeData:GetSkillType(),
                            lineTypeIcon = lineTypeIcon,
                            lineType = skillTypeData:GetName(),
                            line = skillLineData:GetName(),
                            lineId = skillLineData:GetId(),
                            parentAbilityId = parentAbilityId,
                            abilityId = abilityId,
                            name = progressionData:GetName(),
                            icon = progressionData:GetIcon(),
                            isActive = true,
                            isUltimate = skillData:IsUltimate(),
                            descriptionHeader = (descriptionHeader ~= "" and descriptionHeader or nil),
                            description = GetAbilityDescription(abilityId, 4),
                            morph = GetAbilityNewEffects(GetAbilityNewEffectLines(abilityId)),
                            -- stats
                            castType = castType,
                            castTimeValue = castTimeValue,
                            targetType = targetType,
                            targetValue = targetValue,
                            rangeType = rangeType,
                            rangeValue = rangeValue,
                            distanceType = distanceType,
                            distanceValue = distanceValue,
                            durationType = durationType,
                            durationValue = durationValue,
                            cooldownType = cooldownType,
                            cooldownValue = cooldownValue,
							costsType = GetString(SI_ABILITY_TOOLTIP_RESOURCE_COST_LABEL),
                            costsTable = costsTable,
                            rolesType = rolesType,
                            rolesValue = rolesValue
                        })
                    end
                else
                    local numRanks = (not skillData:IsCompanionSkill() and skillData:GetNumRanks() or 1)
                    local progressionData = skillData:GetProgressionData(numRanks)
                    local descriptionHeader = GetAbilityDescriptionHeader(progressionData:GetAbilityId())
                    table.insert(dest[curLang], {
                        lineTypeId = skillTypeData:GetSkillType(),
                        lineTypeIcon = lineTypeIcon,
                        lineType = skillTypeData:GetName(),
                        line = skillLineData:GetName(),
                        lineId = skillLineData:GetId(),
                        abilityId = progressionData:GetAbilityId(),
                        name = progressionData:GetName(),
                        icon = progressionData:GetIcon(),
                        isActive = false,
                        isUltimate = false,
                        descriptionHeader = (descriptionHeader ~= "" and descriptionHeader or nil),
                        description = GetAbilityDescription(progressionData:GetAbilityId(), numRanks)
                    })
                end
            end
        end
    end
end

local function _dumpSkills()
    vars.skills[curLang] = {}

    _dumpSkillsFrom(SKILLS_DATA_MANAGER, vars.skills)
end

local function _dumpCompSkills()
    local curCompId = GetActiveCollectibleByType(COLLECTIBLE_CATEGORY_TYPE_COMPANION)
    if curCompId > 0 then --  and ZO_IsElementInNumericallyIndexedTable(COMPANIONS_IDS, curCompId)
        d('dumping skills for companion: ' .. GetCollectibleName(curCompId))
        if vars.compskills_raw[curCompId] == nil then vars.compskills_raw[curCompId] = {} end
        vars.compskills_raw[curCompId][curLang] = {}
        _dumpSkillsFrom(COMPANION_SKILLS_DATA_MANAGER, vars.compskills_raw[curCompId])
    else
        d('failed: no active companion!')
    end
end

local function _finalizeCompanionSkills()
    vars.compskills[curLang] = {}
    for _, compdata in pairs(vars.compskills_raw) do for _, item in ipairs(compdata[curLang]) do table.insert(vars.compskills[curLang], item) end end
end

local function _dumpSets()
    vars.sets[curLang] = {}

    for _, item in ipairs(ItemBrowserData.items) do
        local r = CreateEntryFromRaw(item)
        vars.sets[curLang][r.setId] = {
            setId = r.setId,
            name = r.name,
            bonuses = r.bonuses,
            subname = r.subname,
            itemType = r.itemType,
            source = r.source,
            sourceType = GetZoneTypeName(r.zoneType),
            maxEquipped = r.maxEquipped,
            icon = r.icon
        }
    end
end

local function dumpSkills()
    _dumpSkills()
    ReloadUI("ingame")
end

local function dumpCompanionSkills() _dumpCompSkills() end

local function finalizeCompanionSkills()
    _finalizeCompanionSkills()
    ReloadUI("ingame")
end

local function dumpSets()
    _dumpSets()
    ReloadUI("ingame")
end

local function dumpChampPoints()
    _dumpChampPoints()
    ReloadUI("ingame")
end

local function dumpAll()
    d('dumping all data except companion skills...')
    _dumpSkills()
    _dumpSets()
    _dumpChampPoints()
    _finalizeCompanionSkills()
    ReloadUI("ingame")
end

local function OnAddOnLoaded(eventCode, addonName)
    if (addonName ~= "DataDumper") then return end

    EVENT_MANAGER:UnregisterForEvent("DataDumper", EVENT_ADD_ON_LOADED)

    vars = ZO_SavedVars:NewAccountWide("DataDumperSavedVariables", 6, nil, defaults, nil, "$InstallationWide")
    vars['APIVersion'] = GetAPIVersion()

    SLASH_COMMANDS['/ddcompanion'] = dumpCompanionSkills
    SLASH_COMMANDS['/ddfinishcompanions'] = finalizeCompanionSkills
    SLASH_COMMANDS['/ddabilities'] = dumpSkills
    SLASH_COMMANDS['/dditemsets'] = dumpSets
    SLASH_COMMANDS['/ddchampoints'] = dumpChampPoints
    -- if GetAPIVersion() > 100033 then
    --
    -- end
    SLASH_COMMANDS['/dd'] = function()
        d('Commands: ')
        d('/ddhelp')
        d('/ddcompanion')
        d('/ddfinishcompanions')
        d('/ddabilities')
        d('/dditemsets')
        d('/ddchampoints')
        d('/ddall')
        d('/ddtogglelang')
    end
    SLASH_COMMANDS['/ddhelp'] = function()
        if curLang == 'ru' then
            -- LuaFormatter off
            d('Призывая компаньенов по одному юзать /ddcompanion после каждого призыва, потом заюзать /ddall')
			-- LuaFormatter on
        else
            d('Summon companions one by one, using /ddcompanion after summon, then use /ddall')
        end
    end
    SLASH_COMMANDS['/ddall'] = dumpAll
    SLASH_COMMANDS['/ddtogglelang'] = function() SetCVar("language.2", (curLang == "en" and "ru" or "en")) end
end

EVENT_MANAGER:RegisterForEvent("DataDumper", EVENT_ADD_ON_LOADED, OnAddOnLoaded)
